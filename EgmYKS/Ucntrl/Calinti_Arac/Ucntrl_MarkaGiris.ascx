﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_MarkaGiris.ascx.cs" Inherits="EgmYKS.Ucntrl.Calinti_Arac.Ucntrl_MarkaGiris" %> 
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %> 
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<dx:ASPxFormLayout ID="box1" runat="server">
    <Items> 
        <dx:LayoutGroup ColCount="3" Caption="Araç Marka Tanımlama">
            <Items>
                 <dx:LayoutItem ShowCaption="False" Width="20" ColSpan="3">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                             <dx:ASPxButton ID="btnYeniGiris" runat="server" OnClick="btnYeniGiris_Click" Height="16" Width="16">
                                 <Image Url="../../images/Add_16x16.png"> 
                                 </Image>
                             </dx:ASPxButton>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
                <dx:LayoutItem Caption="Marka">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                             <dx:ASPxComboBox ID="cmbMarka" runat="server" ClientInstanceName="cmbMarka" ValueType="System.String"> 
                                   <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                                        txtNewName.SetText(cmbMarka.GetText()); 
                                        txtValue.SetText(cmbMarka.GetValue()); 
                                        }
                                    " />
                                </dx:ASPxComboBox>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
                  <dx:LayoutItem Caption="Yeni İsim">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                            <dx:ASPxTextBox ID="txtNewName" ClientInstanceName="txtNewName" runat="server"> </dx:ASPxTextBox>
                          
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
                  <dx:LayoutItem ShowCaption="False">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                            <dx:ASPxButton ID="btnEdit" runat="server" Text="Kaydet" OnClick="btnEdit_Click"></dx:ASPxButton>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
            </Items>
        </dx:LayoutGroup>
    </Items>
</dx:ASPxFormLayout>
 <div style="display:none">
       <dx:ASPxTextBox ID="txtValue" ClientInstanceName="txtValue" runat="server" ></dx:ASPxTextBox>
 </div>