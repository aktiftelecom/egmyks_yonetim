﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Calinti_Arac
{
    public partial class Ucntrl_MarkaGiris : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack & Page.IsCallback) return;
            FillDropDown();
        }

        private void FillDropDown()
        {
            DataTable dt = HelperDataLib.Select.S_MARKA("");
            cmbMarka.DataSource = dt;
            cmbMarka.ValueField = "O_ID";
            cmbMarka.TextField = "O_MARKASI";
            cmbMarka.DataBind();
        }

        protected void btnYeniGiris_Click(object sender, EventArgs e)
        {
            cmbMarka.Enabled = false;
            cmbMarka.SelectedIndex = -1;
            txtValue.Text = "";
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            bool sonuc = false;

            if (string.IsNullOrEmpty(txtValue.Text))
            {
                if (string.IsNullOrEmpty(txtNewName.Text))
                {
                    HelperDataLib.MessageBox.Show("Zorunlu Alanları Doldurunuz.");
                    return;
                }
                HelperDataLib.Insert.I_MARKA(txtNewName.Text, ref sonuc);
            }
            else
            {
                if (string.IsNullOrEmpty(txtNewName.Text))
                {
                    HelperDataLib.MessageBox.Show("Zorunlu Alanları Doldurunuz.");
                    return;
                }
                HelperDataLib.Update.U_MARKA(Convert.ToInt32(txtValue.Text), txtNewName.Text, ref sonuc);
            }
            if (sonuc)
            {
                cmbMarka.SelectedIndex = -1;
                txtNewName.Text = "";
                txtValue.Text = "";
                FillDropDown();
                cmbMarka.Enabled = true;
            }
        }
    }
}