﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_GuvenlikSirket.ascx.cs" Inherits="EgmYKS.Ucntrl.Guvenlik.Ucntrl_GuvenlikSirket" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<style type="text/css">
    .auto-style1 {
        width: 33%;
    }

    .auto-style2 {
        width: 33%;
    }

    .auto-style4 {
        width: 50%;
    }

    .auto-style7 {
        width: 30%;
    }

    .auto-style8 {
        width: 40%;
    }

    .auto-style9 {
        width: 15%;
    }

    .auto-style10 {
        width: 35%;
    }
</style>
<h3>
    <i class="fa fa-users"></i>Güvenlik Şirket 
</h3>
<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
    <div class="alert alert-success alert-dismissable" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
        İşleminiz başarı ile gerçekleşti.
    </div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
    <div class="alert alert-danger" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
    hata aldı!
    </div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
    <div class="alert alert-warning" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
    alanları yazınız!
    </div>
</asp:Panel>

<dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Yeni Şirket" ShowCollapseButton="false"  Collapsed="true" Theme="MetropolisBlue" Width="100%">
    <ContentPaddings Padding="1px" />
    <PanelCollection>
        <dx:PanelContent runat="server">
              
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label10" runat="server" Text="Şirket Adı *"></asp:Label>
                        <dx:ASPxTextBox ID="txtSirketAdi" runat="server"  Theme="Moderno" Width="100%"></dx:ASPxTextBox> 
                     
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="İrtibat İsmi *"></asp:Label>
                        <dx:ASPxTextBox ID="txtirtibatismi" runat="server" Theme="Moderno" Width="100%"></dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                         <asp:Label ID="Label3" runat="server" Text="İrtibat Telefonu *"></asp:Label>
                        <dx:ASPxTextBox ID="txtirtibatTel" runat="server"  Theme="Moderno" Width="100%">
                                  
                                </dx:ASPxTextBox>

                       

                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="İlçesi *"></asp:Label>
                         <dx:ASPxComboBox ID="cmbIlce" runat="server" ClientInstanceName="cmbIlce" ValueType="System.String"  Theme="Moderno" Width="100%">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnIlceChanged(s); }" />
                                </dx:ASPxComboBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Text="Mahallesi *"></asp:Label>
                         <dx:ASPxComboBox ID="cmbMahalle" runat="server" ClientInstanceName="cmbMahalle" ValueType="System.String"  Theme="Moderno" Width="100%"
                                    OnCallback="cmbMahalle_Callback" EnableCallbackMode="true">
                                </dx:ASPxComboBox>
                    </td>
                    <td class="auto-style1">
                           <asp:Label ID="Label9" runat="server" Text="Sokak *"></asp:Label>
                         <dx:ASPxTextBox ID="txtSokak" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                   
                    <td class="auto-style1">
                        <asp:Label ID="Label6" runat="server" Text="Sahibi Telefonu *"></asp:Label>
                        <dx:ASPxTextBox ID="txtSahibiTelefonu" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                          <asp:Label ID="Label11" runat="server" Text="Telefon Diğer *"></asp:Label>
                        <dx:ASPxTextBox ID="txtTelefonDiger" runat="server"  Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                     <td class="auto-style1"> 
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label7" runat="server" Text="Telefon Diğer 2 *"></asp:Label>
                        <dx:ASPxTextBox ID="txtTelefonDiger2" runat="server"  Theme="Moderno" Width="100%">
                                    
                                </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label8" runat="server" Text="Kullanıcı Adı *"></asp:Label>
                        <dx:ASPxTextBox ID="txtKullaniciAdi" runat="server"  Theme="Moderno" Width="100%"></dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                      
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                           <asp:Label ID="Label12" runat="server" Text="GSM *"></asp:Label>
                       <dx:ASPxTextBox ID="txtGSM" runat="server"  Theme="Moderno" Width="100%">
                                    
                                </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                         <asp:Label ID="Label13" runat="server" Text="Şifre *"></asp:Label>
                        <dx:ASPxTextBox ID="txtSifre" runat="server" Password="true"  Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                          <asp:Label ID="Label14" runat="server" Text="Faks *"></asp:Label>
                       <dx:ASPxTextBox ID="txtFaks" runat="server"  Theme="Moderno" Width="100%">
                                    
                                </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                          <asp:Label ID="Label15" runat="server" Text="Şifre Tekrar *"></asp:Label>
                          <dx:ASPxTextBox ID="txtSifreTekrar" runat="server" Password="true"  Theme="Moderno" Width="100%">
                                    
                                </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" colspan="3">
                          <asp:Label ID="Label5" runat="server" Text="Adres *"></asp:Label>
                         <dx:ASPxMemo ID="txtAdress" runat="server"  Theme="Moderno" Width="100%" Border-BorderStyle="Double">
                                </dx:ASPxMemo>
                    </td>
                </tr>
                 
                <tr>
                    <td class="auto-style1" colspan="3">
                          
                        <dx:ASPxButton ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click" Theme="Moderno"></dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxRoundPanel>

 
<script type="text/javascript">
    function OnIlceChanged(cmbIlce) {
        cmbMahalle.PerformCallback(cmbIlce.GetValue().toString());
    }
</script>
