﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_Alarm.ascx.cs" Inherits="EgmYKS.Ucntrl.Calinti_Arac.Ucntrl_Alarm" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<h3>
    <i class="fa fa-users"></i>Alarm Tanımı
</h3>
 <asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
<div class="alert alert-success alert-dismissable" style="width: 100%">
    <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
    İşleminiz başarı ile gerçekleşti.
</div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
<div class="alert alert-danger" style="width: 100%">
    <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
    hata aldı!
</div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
<div class="alert alert-warning" style="width: 100%">
    <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
    alanları yazınız!
</div>
</asp:Panel>

<div style="width: 100%">
    <asp:Label ID="Label1" runat="server" Text="Alarm"></asp:Label>
    <dx:ASPxTextBox ID="txtAlarm" runat="server" Theme="Moderno" Width="100%">
    </dx:ASPxTextBox> 

    <asp:Label ID="Label2" runat="server" Text="ALT OLAY ADI"></asp:Label>
    <dx:ASPxComboBox ID="cmbAltOlay" runat="server" Theme="Moderno" Width="100%"></dx:ASPxComboBox>
     
    <dx:ASPxCheckBox ID="chDurum" runat="server" Text="Durum" Theme="MetropolisBlue">
    </dx:ASPxCheckBox>

    <table style="width:100%;">
        <tr>
            <td>
                <dx:ASPxButton ID="btnKaydet" runat="server" Text="Kaydet" Theme="Moderno" OnClick="btnKaydet_Click">
                </dx:ASPxButton>
            </td>
            <td>&nbsp;</td>
            <td align="right">
                <dx:ASPxButton ID="btnYeni" runat="server" Text="Yeni" Theme="Moderno" OnClick="btnYeni_Click">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
</div>

<dx:ASPxGridView ID="gridview1" runat="server" AutoGenerateColumns="False" 
    EnableTheming="True" Theme="Moderno" Width="100%" KeyFieldName="A_ID">
    <Columns>
         <dx:GridViewDataTextColumn Caption="Seç" VisibleIndex="7" Width="2%">
            <DataItemTemplate> 
                    <dx:ASPxButton ID="btnEdit" runat="server" OnCommand="btnEdit_Command"  CommandArgument="<%# Bind('A_ID')%>" RenderMode="Link">
                    <Image Url="~/images/Show_16x16.png">
                    </Image>
                </dx:ASPxButton> 
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>

         <dx:GridViewDataTextColumn Caption="Sil" VisibleIndex="7" Width="2%" >
            <DataItemTemplate>
                  <dx:ASPxButton ID="btnDelete" runat="server" OnCommand="btnDelete_Command"  CommandArgument="<%# Bind('A_ID') %>" RenderMode="Link">
                    <ClientSideEvents Click="function(s, e) {
		                                                e.processOnServer = confirm('Silmek istiyor musunuz?');
                                                }" />
                    <Image Url="~/images/DeleteList2_16x16.png">
                    </Image>
                </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataColumn FieldName="A_ADI" Caption="ALARM"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="A_IHBAR" Caption="ALT OLAY ADI"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="DURUM" Caption="DURUM"></dx:GridViewDataColumn>

    </Columns>
    <SettingsBehavior AllowFocusedRow="True" />
    <SettingsPager>
        <Summary Text="Sayfa {0} / {1} ({2} veri)" />
    </SettingsPager>
    <Settings ShowFilterRow="True" />
    <SettingsText EmptyDataRow="Veri yok..." />
    <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
</dx:ASPxGridView>

  