﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Guvenlik
{
    public partial class Ucntrl_GuvenlikMusteri : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                YETKIKONTROL();
            } 
            FillGrid();
            if (!IsPostBack & !Page.IsCallback)
            {
                Ilceler();
                FillSirket();
            }
        }

        
        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("202", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (YAZ == true)
                {

                    DevExpress.Web.ASPxMenu.MenuItem EXCEL = this.ASPxMenu1.Items.FindByName("EXCEL");
                    EXCEL.Enabled = true;

                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }

        private void FillGrid()
        {
            DataTable dt = HelperDataLib.Select.S_GET_GUVENLIK_MUSTERI("");
            gridview1.DataSource = dt;
            gridview1.DataBind();
             
        }

        
        protected void ASPxMenu1_ItemClick(object source, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
        {
            if (e.Item.Name == "KAYDET")
            {
                INSERTEDIT();
            }
            if (e.Item.Name == "SATIR")
            {
                Session["SS_Islem_SatirSayisi"] = e.Item.Text;
                gridview1.SettingsPager.PageSize = Convert.ToInt32(Session["SS_Islem_SatirSayisi"]);
                ASPxMenu1.Items[2].Text = "Satır Sayısı" + "(" + Session["SS_Islem_SatirSayisi"].ToString() + ")";
            }
            if (e.Item.Name == "EXCEL")
            {
                ASPxGridViewExporter1.GridViewID = "ASPxGridView1";
                ASPxGridViewExporter1.FileName = "Raporları" + "_" + DateTime.Now;
                ASPxGridViewExporter1.WriteXlsxToResponse();
            } 
            if (e.Item.Name == "TEMIZLE")
            {
                TEMIZLE();
            }
        }

        private void INSERTEDIT()
        {
            if (string.IsNullOrEmpty(txtMusteriismi.Text) | string.IsNullOrEmpty(txtGirisTarihi.Text) | string.IsNullOrEmpty(txtCikisTarihi.Text) |  string.IsNullOrEmpty(txtCalismaGrubu.Text) | string.IsNullOrEmpty(txtTelefonu.Text) | string.IsNullOrEmpty(cmbGuvenlikSirketi.Text) | string.IsNullOrEmpty(txtSahibi.Text) | string.IsNullOrEmpty(txtSahibiTelefonu.Text) | string.IsNullOrEmpty(cmbIlce.Text) | string.IsNullOrEmpty(cmbMahalle.Text) | string.IsNullOrEmpty(txtSokak.Text) | string.IsNullOrEmpty(txtSiteAdi.Text) | string.IsNullOrEmpty(txtBinaAdi.Text) |  string.IsNullOrEmpty(txtBinaNo.Text) | string.IsNullOrEmpty(txtDaireNo.Text) | string.IsNullOrEmpty(txtMusteriismi.Text) | string.IsNullOrEmpty(txtAdres.Text))
            {
                Warning.Visible = true;
                return;
            }

            if (Session["Musteriid"] == null | Session["Musteriid"] == "")
            {
                INSERT();
            }
            else
            {
                EDIT();
            }
        }

        private void EDIT()
        {
            MESAJKAPAT();
            bool sonuc = false;
            int _param0 = HelperDataLib.Methods.ObjectStringtoint(Session["Musteriid"]);
            string _param1 = HelperDataLib.Methods.ObjectnullorEmpty(cmbGuvenlikSirketi.Value);
            string _param2 = HelperDataLib.Methods.ObjectnullorEmpty(txtMusteriismi.Text);
            DateTime _param3 = HelperDataLib.Methods.ObjectMaxDatetime(txtGirisTarihi.Text);
            DateTime _param4 = HelperDataLib.Methods.ObjectMinDatetime(txtCikisTarihi.Text);
            string _param5 = HelperDataLib.Methods.ObjectnullorEmpty(txtCalismaGrubu.Text);
            string _param6 = HelperDataLib.Methods.ObjectnullorEmpty(txtTelefonu.Text);
            string _param7 = HelperDataLib.Methods.ObjectnullorEmpty(txtSahibi.Text);
            string _param8 = HelperDataLib.Methods.ObjectnullorEmpty(txtSahibiTelefonu.Text);
            string _param9 = HelperDataLib.Methods.ObjectnullorEmpty(cmbIlce.Value);
            string _param10 = HelperDataLib.Methods.ObjectnullorEmpty(cmbMahalle.Value);
            string _param11 = HelperDataLib.Methods.ObjectnullorEmpty(txtSokak.Text);
            string _param12 = HelperDataLib.Methods.ObjectnullorEmpty(txtSiteAdi.Text);
            string _param13 = HelperDataLib.Methods.ObjectnullorEmpty(txtBinaAdi.Text);
            string _param14 = HelperDataLib.Methods.ObjectnullorEmpty(txtBinaNo.Text);
            string _param15 = HelperDataLib.Methods.ObjectnullorEmpty(txtDaireNo.Text);
            string _param16 = HelperDataLib.Methods.ObjectnullorEmpty(txtAdres.Text);
            int _param17 = Convert.ToInt32(chDurum.Checked);

            HelperDataLib.Update.U_GUVENLIKMUSTERI(_param0, _param1, _param2, _param3, _param4, _param5, _param6, _param7, _param8, _param9, _param10, _param11, _param12, _param13, _param14, _param15, _param16, _param17, ref sonuc);

            if (sonuc == true)
            {
                succes.Visible = true;
                TEMIZLE();
                FillGrid();
            }
            else
            {
                error.Visible = true;
            }
        }

        private void INSERT()
        {
            MESAJKAPAT();
            bool sonuc = false;
            string _param1 = HelperDataLib.Methods.ObjectnullorEmpty(cmbGuvenlikSirketi.Value);
            string _param2 = HelperDataLib.Methods.ObjectnullorEmpty(txtMusteriismi.Text);
            DateTime _param3 = HelperDataLib.Methods.ObjectMaxDatetime(txtGirisTarihi.Text);
            DateTime _param4 = HelperDataLib.Methods.ObjectMinDatetime(txtCikisTarihi.Text);
            string _param5 = HelperDataLib.Methods.ObjectnullorEmpty(txtCalismaGrubu.Text);
            string _param6 = HelperDataLib.Methods.ObjectnullorEmpty(txtTelefonu.Text);
            string _param7 = HelperDataLib.Methods.ObjectnullorEmpty(txtSahibi.Text);
            string _param8 = HelperDataLib.Methods.ObjectnullorEmpty(txtSahibiTelefonu.Text);
            string _param9 = HelperDataLib.Methods.ObjectnullorEmpty(cmbIlce.Value);
            string _param10 = HelperDataLib.Methods.ObjectnullorEmpty(cmbMahalle.Value);
            string _param11 = HelperDataLib.Methods.ObjectnullorEmpty(txtSokak.Text);
            string _param12 = HelperDataLib.Methods.ObjectnullorEmpty(txtSiteAdi.Text);
            string _param13 = HelperDataLib.Methods.ObjectnullorEmpty(txtBinaAdi.Text);
            string _param14 = HelperDataLib.Methods.ObjectnullorEmpty(txtBinaNo.Text);
            string _param15 = HelperDataLib.Methods.ObjectnullorEmpty(txtDaireNo.Text);
            string _param16 = HelperDataLib.Methods.ObjectnullorEmpty(txtAdres.Text);
            int _param17 = Convert.ToInt32(chDurum.Checked);

            HelperDataLib.Insert.I_GUVENLIKMUSTERI(_param1, _param2, _param3, _param4, _param5, _param6, _param7, _param8, _param9, _param10, _param11, _param12, _param13, _param14, _param15, _param16, _param17,ref sonuc);
            if (sonuc)
            {
                succes.Visible = true;
                TEMIZLE();
                FillGrid();
            }
            else
            {

                error.Visible = true;
            }
        }

        private void TEMIZLE()
        {
            Session["Musteriid"] = "";
            txtMusteriismi.Text = "";
            txtGirisTarihi.Text = "";
            txtCikisTarihi.Text = "";
            txtCalismaGrubu.Text = "";
            txtTelefonu.Text = "";
            txtSahibi.Text = "";
            txtSahibiTelefonu.Text = "";
            cmbIlce.SelectedIndex = -1;
            cmbMahalle.SelectedIndex = - 1;
            txtSokak.Text = "";
            txtSiteAdi.Text = "";
            txtBinaAdi.Text = "";
            txtBinaNo.Text = "";
            txtDaireNo.Text = "";
            txtAdres.Text = "";
            chDurum.Checked = false;
        }
         
        protected void cmbMahalle_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            Mahalleler(e.Parameter);
        }

        
        void Ilceler()
        {
            try
            {
                cmbIlce.DataSource = HelperDataLib.Select.S_ILCE("34");
                cmbIlce.ValueField = "ILCEID";
                cmbIlce.TextField = "AD";
                cmbIlce.DataBind();
                cmbIlce.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem("(Tümü)", ""));
                cmbIlce.SelectedIndex = 0;
            }
            catch
            { return; }
        }
        void Mahalleler(string ilceid)
        {
            try
            {
                DataTable dt = HelperDataLib.Select.S_Mahalle(ilceid);
                cmbMahalle.DataSource = dt;
                cmbMahalle.ValueField = "I_MAHID";
                cmbMahalle.TextField = "I_ADI";
                cmbMahalle.DataBind();
                cmbMahalle.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem("(Tümü)", ""));
                cmbMahalle.SelectedIndex = 0;

            }
            catch
            { return; }

        }
        void Sokaklar(string ilceid,string mahalleid)
        {
            try
            {
                DataTable dt = HelperDataLib.Select.S_CADDE(ilceid,mahalleid);
                cmbMahalle.DataSource = dt; 
                cmbMahalle.TextField = "I_AD";
                cmbMahalle.DataBind();
                cmbMahalle.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem("(Tümü)", ""));
                cmbMahalle.SelectedIndex = 0;

            }
            catch
            { return; }

        }
        private void FillSirket()
        {
            DataTable dt = HelperDataLib.Select.S_SIRKET();
            cmbGuvenlikSirketi.DataSource = dt;
            cmbGuvenlikSirketi.ValueField = "G_ID";
            cmbGuvenlikSirketi.TextField = "G_SIRKETADI";
            cmbGuvenlikSirketi.DataBind();
        }

        protected void btnSec_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["Musteriid"] = doc;
            DataTable d = HelperDataLib.Select.S_GET_GUVENLIK_MUSTERI(doc);
            txtMusteriismi.Text = d.Rows[0]["GM_MUSTERIISIM"].ToString();
            txtGirisTarihi.Text = d.Rows[0]["GM_GIRISTARIH"].ToString();
            txtCikisTarihi.Text = d.Rows[0]["GM_CIKISTARIH"].ToString();
            txtCalismaGrubu.Text = d.Rows[0]["GM_CALISMAGRUP"].ToString();
            txtTelefonu.Text = d.Rows[0]["GM_TELEFON"].ToString();
            txtSahibi.Text = d.Rows[0]["GM_SAHIBI"].ToString();
            txtSahibiTelefonu.Text = d.Rows[0]["GM_GSM"].ToString();
            cmbIlce.SelectedItem = cmbIlce.Items.FindByValue(d.Rows[0]["GM_ILCE"].ToString());
            Mahalleler(d.Rows[0]["GM_ILCE"].ToString());
            cmbMahalle.SelectedItem = cmbMahalle.Items.FindByValue(d.Rows[0]["GM_MAHALLE"].ToString());
            txtSokak.Text =  d.Rows[0]["GM_SOKAK"].ToString();
            txtSiteAdi.Text = d.Rows[0]["GM_SITE"].ToString();
            txtBinaAdi.Text = d.Rows[0]["GM_BINA"].ToString();
            txtBinaNo.Text = d.Rows[0]["GM_BINANO"].ToString();
            txtDaireNo.Text = d.Rows[0]["GM_DAIRENO"].ToString();
            txtAdres.Text = d.Rows[0]["GM_ADRES"].ToString();
            chDurum.Checked = Convert.ToBoolean(d.Rows[0]["GM_DURUM"].ToString());
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["Musteriid"] = doc;
            DataTable d = HelperDataLib.Select.S_GET_GUVENLIK_MUSTERI(doc);
            txtMusteriismi.Text = d.Rows[0]["GM_MUSTERIISIM"].ToString();
            txtGirisTarihi.Text = d.Rows[0]["GM_GIRISTARIH"].ToString();
            txtCikisTarihi.Text = d.Rows[0]["GM_CIKISTARIH"].ToString();
            txtCalismaGrubu.Text = d.Rows[0]["GM_CALISMAGRUP"].ToString();
            txtTelefonu.Text = d.Rows[0]["GM_TELEFON"].ToString();
            txtSahibi.Text = d.Rows[0]["GM_SAHIBI"].ToString();
            txtSahibiTelefonu.Text = d.Rows[0]["GM_GSM"].ToString();
            cmbIlce.SelectedItem = cmbIlce.Items.FindByValue(d.Rows[0]["GM_ILCE"].ToString());
            cmbMahalle.SelectedItem = cmbMahalle.Items.FindByValue(d.Rows[0]["GM_MAHALLE"].ToString());
            txtSokak.Text =  d.Rows[0]["GM_SOKAK"].ToString();
            txtSiteAdi.Text = d.Rows[0]["GM_SITE"].ToString();
            txtBinaAdi.Text = d.Rows[0]["GM_BINA"].ToString();
            txtBinaNo.Text = d.Rows[0]["GM_BINANO"].ToString();
            txtDaireNo.Text = d.Rows[0]["GM_DAIRENO"].ToString();
            txtAdres.Text = d.Rows[0]["GM_ADRES"].ToString();
            chDurum.Checked = Convert.ToBoolean(d.Rows[0]["GM_DURUM"].ToString());
        }
         
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }

        protected void btnSil_Command(object sender, CommandEventArgs e)
        {
            bool durum = false;
            string doc = e.CommandArgument.ToString();
            HelperDataLib.Delete.D_GUVENLIKMUSTERI(doc, ref durum);
            if (durum == true)
            { 
              
                succes.Visible = true;
                TEMIZLE();
                FillGrid();
            }
            else
            {
                error.Visible = true;
            }
        }

    }
}