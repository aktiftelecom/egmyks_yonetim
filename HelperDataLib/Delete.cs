﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;

namespace HelperDataLib
{
   public class Delete
    {
     
    
       public static void D_USTOLAY(string ID, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {

               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE FROM I_USTOLAY WHERE U_ID=@ID", con);
               com.Parameters.AddWithValue("@ID", ID);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }
       public static void D_ALTOLAY(string ID, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {

               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE FROM I_ALTOLAY WHERE A_ID=@ID", con);
               com.Parameters.AddWithValue("@ID", ID);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }
       public static void D_ALTOLAY_TUM(string ID, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {

               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE FROM I_ALTOLAY WHERE A_UID=@ID", con);
               com.Parameters.AddWithValue("@ID", ID);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }


        public static void D_GUVENLIKSIRKET(string ID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM I_GUVENLIKSIRKET WHERE G_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }


        public static void D_GUVENLIKMUSTERI(string ID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM I_GUVENLIKMUSTERI WHERE GM_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void D_ARACTIPI(string ID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM I_ARACTIPI WHERE AT_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void D_ALARM(string ID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM I_ALARM WHERE A_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void D_MARKA(string ID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM I_MARKA WHERE O_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

    }
}
