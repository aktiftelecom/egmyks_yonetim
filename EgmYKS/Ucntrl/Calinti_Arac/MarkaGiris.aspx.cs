﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Calinti_Arac
{
    public partial class MarkaGiris : System.Web.UI.Page
    {
        private string currentType
        {
            get
            {
                try
                {
                    return Request.QueryString["Key"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack  ) return;

            if (currentType != "")
            {
                DataTable dt = HelperDataLib.Select.S_MARKA(currentType);
                txtNewName.Text = dt.Rows[0]["O_MARKASI"].ToString();
                txtValue.Text = currentType;
            }
            else
            {
                txtNewName.Text = "";
                txtValue.Text = "";
            }

        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            bool sonuc = false;

            if (string.IsNullOrEmpty(txtValue.Text))
            {
                if (string.IsNullOrEmpty(txtNewName.Text))
                {
                    HelperDataLib.MessageBox.Show("Zorunlu Alanları Doldurunuz.");
                    return;
                }
                HelperDataLib.Insert.I_MARKA(txtNewName.Text, ref sonuc);
            }
            else
            {
                if (string.IsNullOrEmpty(txtNewName.Text))
                {
                    HelperDataLib.MessageBox.Show("Zorunlu Alanları Doldurunuz.");
                    return;
                }
                HelperDataLib.Update.U_MARKA(Convert.ToInt32(txtValue.Text), txtNewName.Text, ref sonuc);
            }
            if (sonuc)
            {
                CloseWindow();
            }
        }


        public void CloseWindow()
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script> window.opener.location.href = window.opener.location.href;window.close();</script>"));
        }
    }
}