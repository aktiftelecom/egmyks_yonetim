﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl
{
    public partial class Ucntrl_AltOlayList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               if (!Page.IsPostBack)
               {
                      YETKIKONTROL();
               }
            YUKLE();
            
        }
        void YETKIKONTROL()
        {

            try
            {
                   bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                   HelperDataLib.Select.S_YETKI_GETVALUES("301", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {
                    ASPxGridView1.Columns["Sil"].Visible = true;
                  
                }
                if (YAZ == true)
                {
                    ASPxButton1.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }
        void YUKLE()
        {
            if (Request.QueryString["id"] != null)
            {
                ASPxGridView1.DataSource = HelperDataLib.Select.S_ALTOLAY_YUKLE_WHERE(HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()));
                ASPxGridView1.DataBind();
            }
        }
      
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            MESAJKAPAT();
            bool durum = false;
            if (ASPxTextBox1.Text != "")
            {
                if (Session["alt_id"] == null)
                {
                    HelperDataLib.Insert.I_ALTOLAY(HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()),ASPxTextBox1.Text,ASPxCheckBox1.Checked,ASPxCheckBox2.Checked,ASPxCheckBox3.Checked,ref durum);
                    if (durum == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
                else
                {
                    HelperDataLib.Update.U_ALTOLAY(Session["alt_id"].ToString(), HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()), ASPxTextBox1.Text,ASPxCheckBox1.Checked, ASPxCheckBox2.Checked, ASPxCheckBox3.Checked, ref durum);
                    if (durum == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
            }
        }
        void TEMIZLE()
        {
            Session["alt_id"] = null;
            ASPxTextBox1.Text = "";
            ASPxCheckBox3.Checked = false;
            ASPxCheckBox2.Checked = false;
            ASPxCheckBox1.Checked = false;
        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            TEMIZLE();
        }

        protected void ASPxButton3_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["alt_id"] = doc;
            DataTable d = HelperDataLib.Select.S_ALTOLAY_YUKLE_WHERE_DETAY(doc);
            ASPxTextBox1.Text=d.Rows[0]["A_IHBAR"].ToString();
            ASPxCheckBox1.Checked = Convert.ToBoolean(d.Rows[0]["A_ONCELIK"].ToString());
            ASPxCheckBox2.Checked = Convert.ToBoolean(d.Rows[0]["A_ACIL"].ToString());
            ASPxCheckBox3.Checked = Convert.ToBoolean(d.Rows[0]["A_DURUM"].ToString());
        }

        protected void ASPxButton4_Command(object sender, CommandEventArgs e)
        {
            bool durum = false;
            string doc = e.CommandArgument.ToString();
            HelperDataLib.Delete.D_ALTOLAY(doc,ref durum);
            if (durum == true)
            {
                succes.Visible = true;
                TEMIZLE();
                YUKLE();
            }
            else
            {
                error.Visible = true;
            }
        }

       
    }
}