﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Calinti_Arac
{
    public partial class Ucntrl_Alarm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                YETKIKONTROL();
            }
            YUKLE();
        }

        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("301", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {
                    gridview1.Columns["Sil"].Visible = true;

                }
                if (YAZ == true)
                {
                    btnKaydet.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }

        private void YUKLE()
        {
            FillCombobox();

            DataTable dt = HelperDataLib.Select.S_ALARM("");
            gridview1.DataSource = dt;
            gridview1.DataBind();
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["alarmid"] = doc;
            DataTable d = HelperDataLib.Select.S_ALARM(doc);
            txtAlarm.Text = d.Rows[0]["A_ADI"].ToString();
            cmbAltOlay.SelectedItem = cmbAltOlay.Items.FindByValue(d.Rows[0]["A_ALTOLAYID"].ToString());
            chDurum.Checked =  Convert.ToBoolean(d.Rows[0]["A_DURUM"].ToString());
            
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            bool sonuc = false;
            HelperDataLib.Delete.D_ALARM(e.CommandArgument.ToString(), ref sonuc);
            if (sonuc == true)
            {
                succes.Visible = true;
                TEMIZLE();
                YUKLE();
            }
            else
            {
                error.Visible = true;
            }

        }

        private void TEMIZLE()
        {
            Session["alarmid"] = null;
            txtAlarm.Text = "";
            cmbAltOlay.SelectedIndex = -1;
            chDurum.Checked = false;
        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            MESAJKAPAT();
            bool sonuc = false;

            if (txtAlarm.Text != "" & cmbAltOlay.Text != "")
            {
                if (Session["alarmid"] == null)
                {
                    HelperDataLib.Insert.I_ALARM(txtAlarm.Text, Convert.ToInt32(cmbAltOlay.Value), chDurum.Checked, ref sonuc);
                    if (sonuc)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {

                        error.Visible = true;
                    }

                }
                else
                {
                    HelperDataLib.Update.U_ALARM(Convert.ToInt32(Session["alarmid"].ToString()), txtAlarm.Text,Convert.ToInt32(cmbAltOlay.Value),chDurum.Checked, ref sonuc);
                    if (sonuc == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
            }

        }

        protected void btnYeni_Click(object sender, EventArgs e)
        {
            TEMIZLE();
        }

        private void FillCombobox()
        {
            DataTable dt = HelperDataLib.Select.S_ALTOLAY_YUKLE();
            cmbAltOlay.DataSource = dt;
            cmbAltOlay.TextField = "A_IHBAR";
            cmbAltOlay.ValueField = "A_ID";
            cmbAltOlay.DataBind();

        }
    }
}