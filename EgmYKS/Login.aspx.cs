﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS
{
       public partial class Login : System.Web.UI.Page
       {
              protected void Page_Load(object sender, EventArgs e)
              {
                     try
                     {
                            if (!Page.IsPostBack)
                            {
                                   HttpCookie oKCookie = Request.Cookies["girismail"];
                                   HttpCookie oKCookiesifre = Request.Cookies["girissifre"];

                                   if (oKCookie != null)
                                   {

                                          TextBox1.Text = oKCookie.Value;
                                          CheckBox1.Checked = true;
                                   }
                                   if (oKCookiesifre != null)
                                   {

                                          TextBox2.Text = oKCookiesifre.Value;
                                          if (TextBox2.Text != "")
                                          {
                                                 TextBox2.Text = "******";
                                          }

                                   }
                            }
                     }
                     catch
                     { return; }
              }
              void MesajKapat()
              {
                     Errorpassword.Visible = false;
                     Errorpassword2.Visible = false;
                     Erroreposta.Visible = false;
                     ErrorPasif.Visible = false;
                     chapta.Visible = false;

              }
              protected void Button1_Click(object sender, EventArgs e)
              {
                     try
                     {
                            MesajKapat();
                            bool durum = false;
                            if (TextBox1.Text != "")
                            {
                                   if (TextBox2.Text != "")
                                   {
                                          HelperDataLib.Select.S_KULLANICIGIRIS(TextBox1.Text, TextBox2.Text, CheckBox1.Checked, ref durum);
                                          if (durum == false)
                                          {
                                                 HelperDataLib.MessageBox.Show("Sicil No veya şifre yalnış.Kontrol ederek tekrar deneyiniz!");
                                          }
                                   }
                                   else
                                   {
                                          HelperDataLib.MessageBox.Show("Şifrenizi yazınız!");
                                   }
                            }
                            else
                            {
                                   HelperDataLib.MessageBox.Show("Sicil No bilginizi yazınız!");
                            }
                     }
                     catch
                     { return; }
              }
       }
}