﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarkaGiris.aspx.cs" Inherits="EgmYKS.Ucntrl.Calinti_Arac.MarkaGiris" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %> 
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxFormLayout ID="box1" runat="server">
    <Items> 
        <dx:LayoutGroup ColCount="2" Caption="Araç Tipi Tanımlama">
            <Items>  
                  <dx:LayoutItem Caption="Yeni İsim">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                            <dx:ASPxTextBox ID="txtNewName" ClientInstanceName="txtNewName" runat="server"> </dx:ASPxTextBox> 
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
                  <dx:LayoutItem ShowCaption="False">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer>
                            <dx:ASPxButton ID="btnEdit" runat="server" Text="Kaydet" OnClick="btnEdit_Click"></dx:ASPxButton>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
            </Items>
        </dx:LayoutGroup>
    </Items>
</dx:ASPxFormLayout>
 <div style="display:none">
       <dx:ASPxTextBox ID="txtValue" ClientInstanceName="txtValue" runat="server" ></dx:ASPxTextBox>
 </div>
    </div>
    </form>
</body>
</html>
