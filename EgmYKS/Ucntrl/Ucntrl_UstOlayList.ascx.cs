﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl
{
    public partial class Ucntrl_UstOlayList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               if (!Page.IsPostBack)
               {
                      YETKIKONTROL();
               }
            YUKLE();
            
        }
        void YETKIKONTROL()
        {

            try
            {
                   bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                   HelperDataLib.Select.S_YETKI_GETVALUES("301", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {
                    ASPxGridView1.Columns["Sil"].Visible = true;
                  
                }
                if (YAZ == true)
                {
                    ASPxButton1.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }
        void YUKLE()
        {
            ASPxGridView1.DataSource = HelperDataLib.Select.S_USTOLAY_YUKLE();
            ASPxGridView1.DataBind();
        }
      
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            MESAJKAPAT();
            bool durum = false;
            if (ASPxTextBox1.Text != "")
            {
                if (Session["ust_id"] == null)
                {
                    HelperDataLib.Insert.I_USTOLAY(ASPxTextBox1.Text, ASPxCheckBox1.Checked,false, false, ref durum);
                    if (durum == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
                else
                {
                    HelperDataLib.Update.U_USTOLAY(Session["ust_id"].ToString(), ASPxTextBox1.Text, ASPxCheckBox1.Checked, false, false, ref durum);
                    if (durum == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
            }
        }
        void TEMIZLE()
        {
            Session["ust_id"] = null;
            ASPxTextBox1.Text = "";
            ASPxCheckBox1.Checked = false;
           
        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            TEMIZLE();
        }

        protected void ASPxButton3_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["ust_id"] = doc;
            DataTable d = HelperDataLib.Select.S_USTOLAY_YUKLE_WHERE(doc);
            ASPxTextBox1.Text=d.Rows[0]["U_IHBAR"].ToString();
            ASPxCheckBox1.Checked = Convert.ToBoolean(d.Rows[0]["U_DURUM"].ToString());
            
        }

        protected void ASPxButton4_Command(object sender, CommandEventArgs e)
        {
            bool durum = false;
            string doc = e.CommandArgument.ToString();
            HelperDataLib.Delete.D_USTOLAY(doc,ref durum);
            if (durum == true)
            {
                HelperDataLib.Delete.D_ALTOLAY_TUM(doc,ref durum);
                succes.Visible = true;
                TEMIZLE();
                YUKLE();
            }
            else
            {
                error.Visible = true;
            }
        }

        protected void ASPxButton5_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Response.Redirect("AltOlay.aspx?id=" + HelperDataLib.Encrypt.UrlEncrypt(doc));
        }
    }
}