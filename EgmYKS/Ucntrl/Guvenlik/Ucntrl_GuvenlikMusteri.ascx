﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_GuvenlikMusteri.ascx.cs" Inherits="EgmYKS.Ucntrl.Guvenlik.Ucntrl_GuvenlikMusteri" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>

<style type="text/css">
    .auto-style1 {
        width: 33%;
    }

    .auto-style2 {
        width: 33%;
    }

    .auto-style4 {
        width: 50%;
    }

    .auto-style7 {
        width: 30%;
    }

    .auto-style8 {
        width: 40%;
    }

    .auto-style9 {
        width: 15%;
    }

    .auto-style10 {
        width: 35%;
    }
</style>
<h3>
    <i class="fa fa-users"></i>Güvenlik Müşteri 
</h3>
<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
    <div class="alert alert-success alert-dismissable" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
        İşleminiz başarı ile gerçekleşti.
    </div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
    <div class="alert alert-danger" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
    hata aldı!
    </div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
    <div class="alert alert-warning" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
    alanları yazınız!
    </div>
</asp:Panel>
<dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Müşteri Kartı" ShowCollapseButton="true" Theme="MetropolisBlue" Width="100%">
    <ContentPaddings Padding="1px" />
    <PanelCollection>
        <dx:PanelContent runat="server">
             

            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label10" runat="server" Text="Müşteri İsmi *"></asp:Label>
                        <dx:ASPxTextBox ID="txtMusteriismi" ClientInstanceName="txtMusteriismi" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Giriş Tarihi *"></asp:Label>
                        <dx:ASPxDateEdit ID="txtGirisTarihi" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxDateEdit>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Çıkış Tarihi *"></asp:Label>
                        <dx:ASPxDateEdit ID="txtCikisTarihi" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxDateEdit>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label3" runat="server" Text="Çalışma Grubu *"></asp:Label>
                        <dx:ASPxTextBox ID="txtCalismaGrubu" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Text="Telefonu *"></asp:Label>
                        <dx:ASPxTextBox ID="txtTelefonu" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                           <asp:Label ID="Label11" runat="server" Text="Güvenlik Şirketi *"></asp:Label>
                        <dx:ASPxComboBox ID="cmbGuvenlikSirketi" runat="server" ClientInstanceName="cmbGuvenlikSirketi" 
                            ValueType="System.String" Theme="Moderno" Width="100%">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label5" runat="server" Text="Sahibi *"></asp:Label>
                        <dx:ASPxTextBox ID="txtSahibi" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label6" runat="server" Text="Sahibi Telefonu *"></asp:Label>
                        <dx:ASPxTextBox ID="txtSahibiTelefonu" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1"></td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label7" runat="server" Text="İlçe *"></asp:Label>
                        <dx:ASPxComboBox ID="cmbIlce" runat="server" ClientInstanceName="cmbIlce" ValueType="System.String" Theme="Moderno" Width="100%">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnIlceChanged(s); }" />
                        </dx:ASPxComboBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label8" runat="server" Text="Mahalle *"></asp:Label>
                        <dx:ASPxComboBox ID="cmbMahalle" runat="server" ClientInstanceName="cmbMahalle" Theme="Moderno" Width="100%"
                            OnCallback="cmbMahalle_Callback" EnableCallbackMode="true">
                        </dx:ASPxComboBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label9" runat="server" Text="Sokak *"></asp:Label>
                         <dx:ASPxTextBox ID="txtSokak" runat="server" Theme="Moderno" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="txtSiteAdi" runat="server" Theme="Moderno" NullText="Site Adı *" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="txtBinaAdi" runat="server" Theme="Moderno" NullText="Bina Adı *" Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="txtBinaNo" runat="server" Theme="Moderno" NullText="Bina No *" Width="30%">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="txtDaireNo" runat="server" Theme="Moderno" NullText="Daire No *" Width="30%">
                        </dx:ASPxTextBox>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style1" colspan="3">
                        <dx:ASPxMemo ID="txtAdres" runat="server" Height="40px" Theme="Moderno" Width="100%" ReadOnly="false"
                            NullText="Adres *" NullTextStyle-HorizontalAlign="Center">
                        </dx:ASPxMemo>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" colspan="3">
                        <dx:ASPxCheckBox ID="chDurum" runat="server" Text="Durum" Theme="Aqua">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
            </table>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxRoundPanel>
<dx:ASPxMenu ID="ASPxMenu1" runat="server" EnableTheming="True" ItemAutoWidth="False" Theme="Moderno" Width="100%" AutoPostBack="True" OnItemClick="ASPxMenu1_ItemClick">
    <Items>
        <dx:MenuItem Text="Kaydet" Name="KAYDET">
            <Image Url="~/images/Refresh_16x16.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="Excel'e Aktar" Name="EXCEL" Enabled="False">
            <Image Url="~/images/ExportToXLSX_16x16.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="Satır" Name="SATIR">
            <Items>
                <dx:MenuItem Name="SATIR" Text="10">
                </dx:MenuItem>
                <dx:MenuItem Name="SATIR" Text="15">
                </dx:MenuItem>
                <dx:MenuItem Name="SATIR" Text="20">
                </dx:MenuItem>
                <dx:MenuItem Name="SATIR" Text="25">
                </dx:MenuItem>
                <dx:MenuItem Name="SATIR" Text="50">
                </dx:MenuItem>
                <dx:MenuItem Name="SATIR" Text="100">
                </dx:MenuItem>
            </Items>
            <Image Url="~/images/ListNumbers_16x16.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Name="TEMIZLE" Text="Temizle">
            <Image Url="~/images/DeleteList2_16x16.png">
            </Image>
        </dx:MenuItem>
    </Items>
</dx:ASPxMenu>


<dx:ASPxGridView ID="gridview1" runat="server" AutoGenerateColumns="False"
    EnableTheming="True" Theme="Moderno" Width="100%" KeyFieldName="GM_ID">
    <Columns>
        <dx:GridViewDataTextColumn Caption="id" FieldName="GM_ID" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataComboBoxColumn Visible="false" Caption="Şirket" FieldName="GM_GID" VisibleIndex="1">
            <EditFormSettings Visible="True" />
            <PropertiesComboBox>
                <ValidationSettings>
                    <RequiredField IsRequired="true" ErrorText="Lütfen Şirket Seçiniz." />
                </ValidationSettings>
            </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn Caption="Ad Soyad" FieldName="GM_MUSTERIISIM" Name="AdSoyad" VisibleIndex="2">
            <PropertiesTextEdit>
                <ValidationSettings>
                    <RequiredField IsRequired="true" ErrorText="Lütfen Müşteri İsmi Giriniz." />
                </ValidationSettings>
            </PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Çalışma Grubu" FieldName="GM_CALISMAGRUP" Name="CalismaGrubu" VisibleIndex="2">
            <PropertiesTextEdit>
                <ValidationSettings>
                    <RequiredField IsRequired="true" ErrorText="Lütfen Çalışma Grubu Giriniz." />
                </ValidationSettings>
            </PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Telefon" FieldName="GM_TELEFON" Name="Telefon" VisibleIndex="3">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Sahibi" FieldName="GM_SAHIBI" Name="Sahibi" VisibleIndex="4">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="GSM" FieldName="GM_GSM" Name="Gsm" VisibleIndex="5">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="İlçesi" FieldName="ILCEADI" Name="Ilce" VisibleIndex="6">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Mahallesi" FieldName="MAHALLEADI" Name="Mahalle" VisibleIndex="7">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Sokak" FieldName="GM_SOKAK" Name="Sokak" VisibleIndex="8">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Site" FieldName="GM_SITE" Name="Site" VisibleIndex="9">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Bina" FieldName="GM_BINA" Name="Bina" VisibleIndex="10">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Bina No" FieldName="GM_BINANO" Name="Bina No" VisibleIndex="11">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Daire No" FieldName="GM_DAIRENO" Name="Daire No" VisibleIndex="12">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Adres" FieldName="GM_ADRES" Name="Adres" VisibleIndex="13">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn Visible="false" FieldName="GM_GIRISTARIH" VisibleIndex="20">
            <EditFormSettings Visible="True" />
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataDateColumn Visible="false" FieldName="GM_CIKISTARIH" VisibleIndex="21">
            <EditFormSettings Visible="True" />
        </dx:GridViewDataDateColumn> 
        <dx:GridViewDataTextColumn Caption="Seç" VisibleIndex="42" Width="2%">
            <DataItemTemplate>
                <dataitemtemplate>  
                        </dataitemtemplate>
                <dx:ASPxButton ID="btnSec" runat="server" CommandArgument='<%#Eval("GM_ID") %>' OnCommand="btnSec_Command" RenderMode="Link">
                    <Image Url="~/images/Show_16x16.png">
                    </Image>
                </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Sil" VisibleIndex="43" Width="2%"  >
            <DataItemTemplate>
                <dx:ASPxButton ID="btnSil" runat="server" OnCommand="btnSil_Command"  CommandArgument="<%# Bind('GM_ID') %>" RenderMode="Link">
                    <ClientSideEvents Click="function(s, e) {
		                    e.processOnServer = confirm('Silmek istiyor musunuz?');
                    }" />
                    <Image Url="~/images/DeleteList2_16x16.png">
                    </Image>
                </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>

    </Columns>
    <SettingsBehavior AllowFocusedRow="True" />
    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
    <SettingsText EmptyDataRow="Veri yok..." />
</dx:ASPxGridView>


<dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server">
</dx:ASPxGridViewExporter>



<script type="text/javascript">
    function OnIlceChanged(cmbIlce) {
        cmbMahalle.PerformCallback(cmbIlce.GetValue().toString());
    }

    function OnMahalleChanged(cmbMahalle) {
        cmbSokak.PerformCallback(cmbMahalle.GetValue().toString());
    }
</script>
