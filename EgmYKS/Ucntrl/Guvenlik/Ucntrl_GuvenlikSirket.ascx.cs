﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Guvenlik
{
    public partial class Ucntrl_GuvenlikSirket : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack & Page.IsCallback) return; 
            FillIlce();
        }

        private void FillIlce()
        {
            DataTable dt = HelperDataLib.Select.S_ILCE("34");
            cmbIlce.DataSource = dt;
            cmbIlce.ValueField = "ILCEID";
            cmbIlce.TextField = "AD";
            cmbIlce.DataBind();
        }

        private void FillMahalle(string ilceid)
        {
            DataTable dt = HelperDataLib.Select.S_Mahalle(ilceid);
            cmbMahalle.DataSource = dt;
            cmbMahalle.ValueField = "I_MAHID";
            cmbMahalle.TextField = "I_ADI";
            cmbMahalle.DataBind();
        }

        protected void cmbMahalle_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            FillMahalle(e.Parameter);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSifre.Text) | string.IsNullOrEmpty(txtSifre.Text) | string.IsNullOrEmpty(txtSirketAdi.Text) | string.IsNullOrEmpty(txtirtibatismi.Text) | string.IsNullOrEmpty(txtirtibatTel.Text) | string.IsNullOrEmpty(cmbIlce.Text) | string.IsNullOrEmpty(cmbMahalle.Text) | string.IsNullOrEmpty(txtSokak.Text) | string.IsNullOrEmpty(txtSahibiTelefonu.Text) | string.IsNullOrEmpty(txtTelefonDiger.Text) | string.IsNullOrEmpty(txtTelefonDiger2.Text) | string.IsNullOrEmpty(txtKullaniciAdi.Text) | string.IsNullOrEmpty(txtGSM.Text) | string.IsNullOrEmpty(txtFaks.Text))
            {
                Warning.Visible = true;
                return;
            }
                 
            bool sonuc=false;
            string _param1 = HelperDataLib.Methods.ObjectnullorEmpty(cmbIlce.SelectedItem.Value);
            string _param2 = HelperDataLib.Methods.ObjectnullorEmpty(cmbMahalle.SelectedItem.Value);
            HelperDataLib.Insert.I_GUVENLIKSIRKET(txtSirketAdi.Text, _param1, _param2, txtAdress.Text, txtKullaniciAdi.Text, txtSifre.Text, txtSifreTekrar.Text, txtirtibatTel.Text, txtTelefonDiger.Text, txtTelefonDiger2.Text, txtGSM.Text, txtFaks.Text, ref sonuc);
            succes.Visible = true;
            Temizle();
        }

        private void Temizle()
        {
            txtSirketAdi.Text = "";
            txtAdress.Text = "";
            txtKullaniciAdi.Text = "";
            txtSifre.Text = "";
            txtSifreTekrar.Text = "";
            txtirtibatismi.Text = "";
            txtirtibatTel.Text = "";
            txtTelefonDiger.Text = "";
            txtTelefonDiger2.Text = "";
            txtGSM.Text = "";
            txtFaks.Text = "";
            cmbIlce.SelectedIndex = -1;
            cmbMahalle.SelectedIndex = -1;
            cmbMahalle.DataSource = null;
            cmbMahalle.DataBind();
            txtSokak.Text = "";
            txtSahibiTelefonu.Text = "";
        }
    }
}