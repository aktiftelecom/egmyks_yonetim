﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Calinti_Arac
{
    public partial class AlarmGiris : System.Web.UI.Page
    {
        private string currentType
        {
            get
            {
                try
                {
                    return Request.QueryString["Key"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack | Page.IsCallback) return;
            FillCombobox();

            if (currentType != "")
            {
                DataTable dt = HelperDataLib.Select.S_ALARM(currentType);
                txtAlarmAdi.Text = dt.Rows[0]["A_ADI"].ToString();
                cmbAltOlay.SelectedItem = cmbAltOlay.Items.FindByValue(dt.Rows[0]["A_ALTOLAYID"].ToString());
                cmbDurum.SelectedItem = cmbDurum.Items.FindByText(dt.Rows[0]["DURUM"].ToString());
            } 
        }

        private void FillCombobox()
        {
            DataTable dt = HelperDataLib.Select.S_ALTOLAY_YUKLE();
            cmbAltOlay.DataSource = dt;
            cmbAltOlay.TextField = "A_IHBAR";
            cmbAltOlay.ValueField = "A_ID";
            cmbAltOlay.DataBind();

        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            bool sonuc = false;
            if (string.IsNullOrEmpty(currentType))
            {
                HelperDataLib.Insert.I_ALARM(txtAlarmAdi.Text, Convert.ToInt32(cmbAltOlay.Value), Convert.ToBoolean(Convert.ToInt32(cmbDurum.Value)), ref sonuc);
            }
            else
            {
                HelperDataLib.Update.U_ALARM(Convert.ToInt32(currentType), txtAlarmAdi.Text, Convert.ToInt32(cmbAltOlay.Value), Convert.ToBoolean(Convert.ToInt32(cmbDurum.Value)), ref sonuc);
            }

            if (sonuc)
            { 
                CloseWindow();
            }
        }

        public void CloseWindow()
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script> window.opener.location.href = window.opener.location.href;window.close();</script>"));
        }
    }
}