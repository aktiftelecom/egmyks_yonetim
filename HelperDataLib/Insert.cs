﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;
using System.Data;

namespace HelperDataLib
{
    public class Insert
    {
     
   
        public static void I_USTOLAY(string U_IHBAR, bool U_DURUM, bool U_ONCELIK, bool U_MAIL, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_USTOLAY(U_IHBAR,U_DURUM,U_ONCELIK,U_MAIL) values(@U_IHBAR,@U_DURUM,@U_ONCELIK,@U_MAIL)", con);
                com.Parameters.AddWithValue("@U_IHBAR", U_IHBAR);
                com.Parameters.AddWithValue("@U_DURUM", U_DURUM);
                com.Parameters.AddWithValue("@U_ONCELIK", U_ONCELIK);
                com.Parameters.AddWithValue("@U_MAIL", U_MAIL);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void I_ALTOLAY(string A_UID, string A_IHBAR, bool A_ONCELIK, bool A_ACIL, bool A_DURUM, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_ALTOLAY(A_UID,A_IHBAR,A_ONCELIK,A_ACIL,A_DURUM) values(@A_UID,@A_IHBAR,@A_ONCELIK,@A_ACIL,@A_DURUM)", con);
                com.Parameters.AddWithValue("@A_UID", A_UID);
                com.Parameters.AddWithValue("@A_IHBAR", A_IHBAR);
                com.Parameters.AddWithValue("@A_ONCELIK", A_ONCELIK);
                com.Parameters.AddWithValue("@A_ACIL", A_ACIL);
                com.Parameters.AddWithValue("@A_DURUM", A_DURUM);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

       

    
  

        public static void I_GUVENLIKSIRKET(string G_SIRKETADI, string G_ILCE, string G_MAHALLE, string G_ADRES, string G_KULLANICIADI, string G_SIFRE, string G_IRTIBATISMI, string G_IRTIBATTELEFON, string G_TELEFONDIGER,string G_TELEFONDIGER2,string G_GSM, string G_FAX, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_GUVENLIKSIRKET(G_SIRKETADI,G_ILCE,G_MAHALLE,G_ADRES,G_KULLANICIADI,G_SIFRE,G_IRTIBATISMI,G_IRTIBATTELEFON,G_TELEFONDIGER,G_TELEFONDIGER2,G_GSM,G_FAX) values(@G_SIRKETADI,@G_ILCE,@G_MAHALLE,@G_ADRES,@G_KULLANICIADI,@G_SIFRE,@G_IRTIBATISMI,@G_IRTIBATTELEFON,@G_TELEFONDIGER,@G_TELEFONDIGER2,@G_GSM,@G_FAX)", con);
                com.Parameters.AddWithValue("@G_SIRKETADI", G_SIRKETADI);
                com.Parameters.AddWithValue("@G_ILCE", G_ILCE);
                com.Parameters.AddWithValue("@G_MAHALLE", G_MAHALLE);
                com.Parameters.AddWithValue("@G_ADRES", G_ADRES);
                com.Parameters.AddWithValue("@G_KULLANICIADI", G_KULLANICIADI);
                com.Parameters.AddWithValue("@G_SIFRE", G_SIFRE);
                com.Parameters.AddWithValue("@G_IRTIBATISMI", G_IRTIBATISMI);
                com.Parameters.AddWithValue("@G_IRTIBATTELEFON", G_IRTIBATTELEFON);
                com.Parameters.AddWithValue("@G_TELEFONDIGER", G_TELEFONDIGER);
                com.Parameters.AddWithValue("@G_TELEFONDIGER2", G_TELEFONDIGER2);
                com.Parameters.AddWithValue("@G_GSM", G_GSM);
                com.Parameters.AddWithValue("@G_FAX", G_FAX);

                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }


        public static void I_GUVENLIKMUSTERI(string  GM_GID, string GM_MUSTERIISIM, DateTime GM_GIRISTARIH, DateTime GM_CIKISTARIH, string GM_CALISMAGRUP, string GM_TELEFON, string GM_SAHIBI, string GM_GSM, string GM_ILCE, string GM_MAHALLE, string GM_SOKAK, string GM_SITE, string GM_BINA, string GM_BINANO, string GM_DAIRENO, string GM_ADRES, int GM_DURUM ,  ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_GUVENLIKMUSTERI(GM_GID,GM_MUSTERIISIM,GM_GIRISTARIH,GM_CIKISTARIH,GM_CALISMAGRUP,GM_TELEFON,GM_SAHIBI,GM_GSM,GM_ILCE,GM_MAHALLE,GM_SOKAK,GM_SITE,GM_BINA,GM_BINANO,GM_DAIRENO,GM_ADRES,GM_DURUM) "+
                    "values(@GM_GID, @GM_MUSTERIISIM,@GM_GIRISTARIH,@GM_CIKISTARIH,@GM_CALISMAGRUP,@GM_TELEFON,@GM_SAHIBI,@GM_GSM,@GM_ILCE,@GM_MAHALLE,@GM_SOKAK,@GM_SITE,@GM_BINA,@GM_BINANO,@GM_DAIRENO,@GM_ADRES,@GM_DURUM)", con);
                com.Parameters.AddWithValue("@GM_GID", GM_GID);
                com.Parameters.AddWithValue("@GM_MUSTERIISIM", GM_MUSTERIISIM);
                com.Parameters.AddWithValue("@GM_GIRISTARIH", GM_GIRISTARIH);
                com.Parameters.AddWithValue("@GM_CIKISTARIH", GM_CIKISTARIH);
                com.Parameters.AddWithValue("@GM_CALISMAGRUP", GM_CALISMAGRUP);
                com.Parameters.AddWithValue("@GM_TELEFON", GM_TELEFON);
                com.Parameters.AddWithValue("@GM_SAHIBI", GM_SAHIBI);
                com.Parameters.AddWithValue("@GM_GSM", GM_GSM);
                com.Parameters.AddWithValue("@GM_ILCE", GM_ILCE);
                com.Parameters.AddWithValue("@GM_MAHALLE", GM_MAHALLE);
                com.Parameters.AddWithValue("@GM_SOKAK", GM_SOKAK);
                com.Parameters.AddWithValue("@GM_SITE", GM_SITE);
                com.Parameters.AddWithValue("@GM_BINA", GM_BINA);
                com.Parameters.AddWithValue("@GM_BINANO", GM_BINANO);
                com.Parameters.AddWithValue("@GM_DAIRENO", GM_DAIRENO);
                com.Parameters.AddWithValue("@GM_ADRES", GM_ADRES);
                com.Parameters.AddWithValue("@GM_DURUM", GM_DURUM); 

            
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
           
        }




        public static void I_ARACTIPI(string AT_ADI, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_ARACTIPI (AT_ADI) values(@AT_ADI)", con);
                com.Parameters.AddWithValue("@AT_ADI", AT_ADI);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        
        public static void I_ALARM(string A_ADI, int A_ALTOLAYID, bool A_DURUM, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_ALARM (A_ADI,A_ALTOLAYID,A_DURUM) values(@A_ADI,@A_ALTOLAYID,@A_DURUM)", con);
                com.Parameters.AddWithValue("@A_ADI", A_ADI);
                com.Parameters.AddWithValue("@A_ALTOLAYID", A_ALTOLAYID);
                com.Parameters.AddWithValue("@A_DURUM", A_DURUM);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void I_MARKA(string O_MARKASI, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_MARKA (O_MARKASI) values(@O_MARKASI)", con);
                com.Parameters.AddWithValue("@O_MARKASI", O_MARKASI);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }


    }
}

