﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EgmYKS.Ucntrl.Calinti_Arac
{
    public partial class Ucntrl_AracTipleri : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                YETKIKONTROL();
            }
            YUKLE();
        }

        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("301", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {
                    gridview1.Columns["Sil"].Visible = true;

                }
                if (YAZ == true)
                {
                    btnKaydet.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }

        private void YUKLE()
        {
            DataTable dt = HelperDataLib.Select.S_ARACTIPI("");
            gridview1.DataSource = dt;
            gridview1.DataBind();
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            Session["aractipid"] = doc;
            DataTable d = HelperDataLib.Select.S_ARACTIPI(doc);
            txtAracTipi.Text = d.Rows[0]["AT_ADI"].ToString();
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            bool sonuc = false;
            HelperDataLib.Delete.D_ARACTIPI(e.CommandArgument.ToString(), ref sonuc);
            if (sonuc == true)
            {
                succes.Visible = true;
                TEMIZLE();
                YUKLE();
            }
            else
            {
                error.Visible = true;
            }

        }

        private void TEMIZLE()
        {
            Session["aractipid"] = null;
            txtAracTipi.Text = "";
        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            MESAJKAPAT();
            bool sonuc = false;

            if (txtAracTipi.Text != "")
            {
                if (Session["aractipid"] == null)
                {
                    HelperDataLib.Insert.I_ARACTIPI(txtAracTipi.Text, ref sonuc);
                    if (sonuc)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {

                        error.Visible = true;
                    }

                }
                else
                {
                    HelperDataLib.Update.U_ARACTIPI(Convert.ToInt32(Session["aractipid"].ToString()), txtAracTipi.Text, ref sonuc);
                    if (sonuc == true)
                    {
                        succes.Visible = true;
                        TEMIZLE();
                        YUKLE();
                    }
                    else
                    {
                        error.Visible = true;
                    }
                }
            }

        }

        protected void btnYeni_Click(object sender, EventArgs e)
        {
            TEMIZLE();
        }
       
    }
}